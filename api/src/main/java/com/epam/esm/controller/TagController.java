package com.epam.esm.controller;

import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.DatabaseException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.services.GiftCertificateService;
import com.epam.esm.services.TagService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * Tag controller for connecting with client
 *
 * @author Abduvohid Isroilov
 */
@RestController
public class TagController {

    private final TagService tagService;
    private final GiftCertificateService giftCertificateService;

    public TagController(TagService tagService, GiftCertificateService giftCertificateService) {
        this.tagService = tagService;
        this.giftCertificateService = giftCertificateService;
    }

    /**
     * Getting all tags
     *
     * @return ResponseEntity<?>
     */
    @GetMapping("/tag")
    public ResponseEntity<?> get() {
        return new ResponseEntity<>(tagService.getAll(), HttpStatus.OK);
    }

    /**
     * Getting Tag by its id
     *
     * @param id @PathVariable
     * @return ResponseEntity<?>
     */
    @GetMapping("/tag/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return new ResponseEntity<>(tagService.getById(id), HttpStatus.OK);
    }

    /**
     * Creating new Tag
     *
     * @param tagDto @PathVariable
     * @return ResponseEntity<?>
     */
    @PostMapping("/tag")
    public ResponseEntity<?> create(@RequestBody TagDto tagDto) {
        try {

            if (!saveGiftCertificateAndTag(tagDto))
                throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " Error while creating tag and gifts ");

            return new ResponseEntity<>("Tag is created", HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Deleting Tag by its id
     *
     * @param id @PathVariable
     * @return ResponseEntity<?>
     */
    @DeleteMapping("/tag/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        try {
            tagService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Getting tags by their name
     *
     * @param name @RequestParam
     * @return ResponseEntity<?>
     */
    @GetMapping("/tag/get-by-name/{name}")
    public ResponseEntity<?> getByName(@PathVariable String name) {
        return new ResponseEntity<>(tagService.getTagsByName(name), HttpStatus.OK);
    }


    /**
     * Method for saving gift certificates and tag into the database, @Transactional will rollback if something went wrong
     *
     * @param tagDto TagDto
     * @return Boolean
     */
    @Transactional
    boolean saveGiftCertificateAndTag(TagDto tagDto) {
        long tagId = tagService.create(tagDto);

        if (tagId == 0)
            return false;

        tagDto.getGiftCertificateDtos()
                .forEach(giftCertificateDto -> {
                    if (giftCertificateDto != null) {
                        long giftCertificateId = 0;
                        if (giftCertificateDto.getId() == null) {
                            giftCertificateId = giftCertificateService.create(giftCertificateDto);
                            if (giftCertificateId != 0)
                                giftCertificateService.saveToGiftCertificateTags(giftCertificateId, tagId);
                            else
                                throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " (COULD NOT CREATE GIFT CERTIFICATE ) ");
                        } else
                            giftCertificateService.saveToGiftCertificateTags(giftCertificateDto.getId(), tagId);
                    }
                });
        return true;
    }

}

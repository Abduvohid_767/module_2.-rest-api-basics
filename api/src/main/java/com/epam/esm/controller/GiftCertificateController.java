package com.epam.esm.controller;

import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.exception.DatabaseException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.services.GiftCertificateService;
import com.epam.esm.services.TagService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * GiftCertificate controller for connecting with client
 *
 * @author Abduvohid Isroilov
 */
@RestController
public class GiftCertificateController {

    private final GiftCertificateService giftCertificateService;
    private final TagService tagService;

    public GiftCertificateController(GiftCertificateService giftCertificateService, TagService tagService) {
        this.giftCertificateService = giftCertificateService;
        this.tagService = tagService;
    }

    /**
     * Getting all gift certificates also can sort them using this controller method
     *
     * @param sortByName           @RequestParam
     * @param sortByCreateDate     @RequestParam
     * @param sortByLastUpdateDate @RequestParam
     * @return ResponseEntity<?>
     */
    @GetMapping("/gift_certificate")
    public ResponseEntity<?> get(@RequestParam(name = "sort-by-name-asc", required = false) Boolean sortByName,
                                 @RequestParam(name = "sort-by-create_date-asc", required = false) Boolean sortByCreateDate,
                                 @RequestParam(name = "sort-by-last_update_date-asc", required = false) Boolean sortByLastUpdateDate) {

        if (sortByName != null || sortByCreateDate != null || sortByLastUpdateDate != null)
            return getSorted(sortByName, sortByCreateDate, sortByLastUpdateDate);

        return new ResponseEntity<>(giftCertificateService.getAll(), HttpStatus.OK);
    }

    /**
     * Getting gift certificate by its id
     *
     * @param id @PathVariable
     * @return ResponseEntity<?>
     */
    @GetMapping("/gift_certificate/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return new ResponseEntity<>(giftCertificateService.getById(id), HttpStatus.OK);
    }

    /**
     * Creating new gift certificate
     *
     * @param giftCertificateDto @RequestBody
     * @return ResponseEntity<?>
     */
    @PostMapping("/gift_certificate")
    public ResponseEntity<?> create(@RequestBody GiftCertificateDto giftCertificateDto) {
        try {

            if (!saveGiftCertificateAndTag(giftCertificateDto))
                throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " Error while creating gift and tag!");

            return new ResponseEntity<>("Gift Certificate is created", HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Deleting gift certificate by its id
     *
     * @param id @PathVariable
     * @return ResponseEntity<?>
     */
    @DeleteMapping("/gift_certificate/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        try {
            giftCertificateService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Updating gift certificate only given fields
     *
     * @param id                 @PathVariable
     * @param giftCertificateDto @RequestBody
     * @return ResponseEntity<?>
     */
    @PutMapping("/gift_certificate/{id}")
    public ResponseEntity<?> updateById(@PathVariable Long id, @RequestBody GiftCertificateDto giftCertificateDto) {
        try {
            if (giftCertificateService.updateGiftCertificateById(giftCertificateDto, id))
                return new ResponseEntity<>(giftCertificateDto, HttpStatus.OK);
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " Error while updating gift ");
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Getting gift certificates by tag name
     *
     * @param name @PathVariable
     * @return ResponseEntity<?>
     */
    @GetMapping("/gift_certificate/get-by-tag-name/{name}")
    public ResponseEntity<?> getCertificatesByTagName(@PathVariable String name) {
        return new ResponseEntity<>(giftCertificateService.getGiftCertificatesByTagName(name), HttpStatus.OK);
    }

    /**
     * Getting gift certificates by its description
     *
     * @param description @PathVariable
     * @return ResponseEntity<?>
     */
    @GetMapping("/gift_certificate/get-by-description/{description}")
    public ResponseEntity<?> getCertificatesByDescription(@PathVariable String description) {
        return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateByDescription(description), HttpStatus.OK);
    }

    /**
     * Updating multiple gift certificates
     *
     * @param giftCertificateDtoList @RequestBody
     * @return ResponseEntity<?>
     */
    @PutMapping("/gift_certificate/batch-update")
    public ResponseEntity<?> batchUpdate4giftCertificate(@RequestBody List<GiftCertificateDto> giftCertificateDtoList) {
        return new ResponseEntity<>(giftCertificateService.batchUpdate(giftCertificateDtoList), HttpStatus.OK);
    }

    /**
     * Sorting helper method
     *
     * @param sortByName           Boolean
     * @param sortByCreateDate     Boolean
     * @param sortByLastUpdateDate Boolean
     * @return ResponseEntity<?>
     */
    private ResponseEntity<?> getSorted(Boolean sortByName,
                                        Boolean sortByCreateDate,
                                        Boolean sortByLastUpdateDate) {
        if (sortByName != null) {
            if (sortByCreateDate != null)
                return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByCreateDateAndName(sortByCreateDate, sortByName), HttpStatus.OK);
            if (sortByLastUpdateDate != null)
                return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByLastUpdateDateAndName(sortByLastUpdateDate, sortByName), HttpStatus.OK);
            return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByName(sortByName), HttpStatus.OK);
        }

        if (sortByCreateDate != null)
            return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByCreateDate(sortByCreateDate), HttpStatus.OK);

        if (sortByLastUpdateDate != null)
            return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByLastUpdateDate(sortByLastUpdateDate), HttpStatus.OK);
        return null;
    }

    /**
     * Method for saving gift certificate and tags into the database, @Transactional will rollback if something went wrong
     *
     * @param giftCertificateDto GiftCertificateDto
     * @return Boolean
     */
    @Transactional
    boolean saveGiftCertificateAndTag(GiftCertificateDto giftCertificateDto) {
        long giftCertificateId = giftCertificateService.create(giftCertificateDto);

        if (giftCertificateId == 0)
            throw new RuntimeException("Could not create giftCertificate");

        giftCertificateDto.getTagDtos()
                .forEach(tagDto -> {
                    if (tagDto != null) {
                        long tagId = 0;
                        if (tagDto.getId() == null) {
                            tagId = tagService.create(tagDto);
                            if (tagId != 0)
                                giftCertificateService.saveToGiftCertificateTags(giftCertificateId, tagId);
                            else
                                throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( COULD NOT CREATE TAG ) ");
                        } else
                            giftCertificateService.saveToGiftCertificateTags(giftCertificateId, tagDto.getId());
                    }
                });
        return true;
    }
}

package com.epam.esm.mapper;

import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * GiftCertificate Object mapper for the converting gift_certificate model to dto
 * And vice versa
 *
 * @author Abduvohid Isroilov
 */
@Component
public class GiftCertificateObjectMapper implements BaseObjectMapper<GiftCertificate, GiftCertificateDto> {

    /**
     * @param dto
     * @return GiftCertificate
     * @value GiftCertificateDto
     */
    @Override
    public GiftCertificate dtoToModel(GiftCertificateDto dto) {
        GiftCertificate giftCertificate = new GiftCertificate();

        TagObjectObjectMapper tagObjectMapper = new TagObjectObjectMapper();

        giftCertificate.setId(dto.getId());
        giftCertificate.setDescription(dto.getDescription());
        giftCertificate.setName(dto.getName());
        giftCertificate.setPrice(dto.getPrice());
        giftCertificate.setDuration(dto.getDuration());
        giftCertificate.setCreate_date(dto.getCreate_date());
        giftCertificate.setLast_update_date(dto.getLast_update_date());

        List<Tag> tags = new LinkedList<>();

        dto.getTagDtos()
                .forEach(tagDto -> {
                    if (tagDto != null) tags.add(tagObjectMapper.dtoToModel(tagDto));
                });

        if (!tags.isEmpty())
            giftCertificate.setTags(tags);

        return giftCertificate;
    }

    /**
     * @param model
     * @return GiftCertificateDto
     * @value GiftCertificate
     */
    @Override
    public GiftCertificateDto modelToDto(GiftCertificate model) {
        GiftCertificateDto giftCertificateDto = new GiftCertificateDto();
        TagObjectObjectMapper tagObjectMapper = new TagObjectObjectMapper();

        giftCertificateDto.setId(model.getId());
        giftCertificateDto.setName(model.getName());
        giftCertificateDto.setDuration(model.getDuration());
        giftCertificateDto.setPrice(model.getPrice());
        giftCertificateDto.setDescription(model.getDescription());
        giftCertificateDto.setCreate_date(model.getCreate_date());
        giftCertificateDto.setLast_update_date(model.getLast_update_date());

        List<TagDto> tagDtos = new LinkedList<>();

        model.getTags()
                .forEach(tag -> {
                    if (tag != null)
                        tagDtos.add(tagObjectMapper.modelToDto(tag));
                });

        if (!tagDtos.isEmpty())
            giftCertificateDto.setTagDtos(tagDtos);

        return giftCertificateDto;
    }

}

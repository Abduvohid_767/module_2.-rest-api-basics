package com.epam.esm.services;

import com.epam.esm.dao.BaseDao;
import com.epam.esm.dto.BaseDto;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.mapper.BaseObjectMapper;
import com.epam.esm.model.BaseModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Base Service for the common CRUD operations
 * <R> should extends BaseDao,<O> should extends BaseMapper, <M> should extends BaseModel, <D> should extends BaseDto
 * All methods will call their dependent repository
 *
 * @param <R>
 * @param <O>
 * @param <M>
 * @param <D>
 * @author Abduvohid Isroilov
 */
public abstract class BaseService<R extends BaseDao<M>, O extends BaseObjectMapper<M, D>,
        M extends BaseModel, D extends BaseDto> {

    private R repository;
    private O mapper;

    public BaseService(R repository, O mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public R getRepository() {
        return repository;
    }

    public O getMapper() {
        return mapper;
    }

    /**
     * Generic get all method
     *
     * @return List<D>
     */
    public List<D> getAll() {
        return repository.getAll()
                .stream().map(model -> mapper.modelToDto(model))
                .collect(Collectors.toList());
    }

    /**
     * Generic create(D input) method
     *
     * @param input Dto
     * @return boolean
     */
    public long create(D input) {
        M model = mapper.dtoToModel(input);
        return repository.save(model);
    }

    /**
     * Generic getById(long id) method for getting data by id from database
     *
     * @param id long
     * @return <D>
     */
    public D getById(long id) {
        M result = repository.get(id).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH ID ( " + id + " ) "));
        return mapper.modelToDto(result);
    }

    /**
     * Generic deleteByName(String name) method for deleting data from database using name
     *
     * @param name String
     */
    @Transactional
    public void deleteByName(String name) {
        M result = repository.getByName(name).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH NAME ( " + name + " ) "));
        repository.delete(result.getId());
    }

    /**
     * Generic deleteById(long id) method for deleting data from database using id
     *
     * @param id long
     */
    @Transactional
    public void deleteById(long id) {
        M result = repository.get(id).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH ID ( " + id + " ) "));
        repository.delete(result.getId());
    }

}

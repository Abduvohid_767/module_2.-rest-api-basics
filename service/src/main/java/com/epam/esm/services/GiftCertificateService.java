package com.epam.esm.services;

import com.epam.esm.dao.GiftCertificateDaoImpl;
import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.mapper.GiftCertificateObjectMapper;
import com.epam.esm.model.GiftCertificate;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * GiftCertificateService class that extends Base Service which provide CRUD operations
 * Coming inputs and outgoing parameters will be dto classes
 */
@Service
public class GiftCertificateService extends BaseService<GiftCertificateDaoImpl,
        GiftCertificateObjectMapper, GiftCertificate, GiftCertificateDto> {

    private final GiftCertificateDaoImpl giftCertificateDaoImpl;

    /**
     * Constructor for Gift Certificate Service
     *
     * @param repository GiftCertificateDaoImpl
     * @param mapper GiftCertificateObjectObjectMapper
     */
    public GiftCertificateService(GiftCertificateDaoImpl repository,
                                  GiftCertificateObjectMapper mapper) {
        super(repository, mapper);
        this.giftCertificateDaoImpl = repository;
    }

    /**
     * Service method for update gift certificate by its id
     *
     * @param giftCertificateDto
     * @param id
     * @return boolean
     */
    public boolean updateGiftCertificateById(GiftCertificateDto giftCertificateDto, long id) {

        GiftCertificate giftCertificate = getMapper().dtoToModel(giftCertificateDto);

        return giftCertificateDaoImpl.updateById(giftCertificate, id);

    }

    /**
     * Batch update method for gift certificate
     *
     * @param giftCertificateDtos
     * @return int[]
     */
    public int[] batchUpdate(List<GiftCertificateDto> giftCertificateDtos) {

        List<GiftCertificate> giftCertificates = new LinkedList<>();
        giftCertificateDtos
                .forEach(dto -> giftCertificates.add(getMapper().dtoToModel(dto)));

        return giftCertificateDaoImpl.batchUpdate(giftCertificates);
    }

    /**
     * Service method for getting gift certificates by tag name
     *
     * @param name
     * @return List<GiftCertificateDto>
     */
    public List<GiftCertificateDto> getGiftCertificatesByTagName(String name) {
        try {

            return giftCertificateDaoImpl.getAllGiftCertificateByTagName(name)
                    .stream()
                    .map(giftCertificate -> getMapper().modelToDto(giftCertificate))
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            throw new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH TAG NAME ( " + name + " ) ");

        }

    }

    /**
     * Service for getting gift certificates by it description
     *
     * @param description
     * @return List<GiftCertificateDto>
     */
    public List<GiftCertificateDto> getAllGiftCertificateByDescription(String description) {

        return giftCertificateDaoImpl.getAllGiftCertificateByDescription(description)
                .stream()
                .map(giftCertificate -> getMapper().modelToDto(giftCertificate))
                .collect(Collectors.toList());

    }

    /**
     * Service method for getting gift certificate in order by name
     *
     * @param isASC
     * @return List<GiftCertificateDto>
     */
    public List<GiftCertificateDto> getAllGiftCertificateOrderByName(boolean isASC) {
        return giftCertificateDaoImpl.getAllGiftCertificateOrderByName(isASC)
                .stream()
                .map(giftCertificate -> getMapper().modelToDto(giftCertificate))
                .collect(Collectors.toList());
    }

    /**
     * Service method for getting gift certificate in order by create date
     *
     * @param isASC
     * @return List<GiftCertificateDto>
     */
    public List<GiftCertificateDto> getAllGiftCertificateOrderByCreateDate(boolean isASC) {
        return giftCertificateDaoImpl.getAllGiftCertificateOrderByCreateDate(isASC)
                .stream()
                .map(giftCertificate -> getMapper().modelToDto(giftCertificate))
                .collect(Collectors.toList());
    }

    /**
     * Service method for getting gift certificate in order by last update date
     *
     * @param isASC
     * @return List<GiftCertificateDto>
     */
    public List<GiftCertificateDto> getAllGiftCertificateOrderByLastUpdateDate(boolean isASC) {
        return giftCertificateDaoImpl.getAllGiftCertificateOrderByLastUpdateDate(isASC)
                .stream()
                .map(giftCertificate -> getMapper().modelToDto(giftCertificate))
                .collect(Collectors.toList());
    }

    /**
     * Service method for getting gift certificates in order create date and its name
     *
     * @param isASCCreateDate
     * @param isASCName
     * @return List<GiftCertificateDto>
     */
    public List<GiftCertificateDto> getAllGiftCertificateOrderByCreateDateAndName(boolean isASCCreateDate, boolean isASCName) {
        return giftCertificateDaoImpl.getAllGiftCertificateOrderByCreateDateAndName(isASCCreateDate, isASCName)
                .stream()
                .map(giftCertificate -> getMapper().modelToDto(giftCertificate))
                .collect(Collectors.toList());
    }

    /**
     * Service method for getting gift certificates in order last update and name
     *
     * @param isASCLastUpdate
     * @param isASCName
     * @return List<GiftCertificateDto>
     */
    public List<GiftCertificateDto> getAllGiftCertificateOrderByLastUpdateDateAndName(boolean isASCLastUpdate, boolean isASCName) {
        return giftCertificateDaoImpl.getAllGiftCertificateOrderByLastUpdateDateAndName(isASCLastUpdate, isASCName)
                .stream()
                .map(giftCertificate -> getMapper().modelToDto(giftCertificate))
                .collect(Collectors.toList());
    }

    /**
     * Service method for saving gift certificate and tag ids into giftCertificateTags table in database
     *
     * @param giftCertificateId
     * @param tagId
     * @return List<GiftCertificateDto>
     */
    public int saveToGiftCertificateTags(long giftCertificateId, long tagId) {
        return giftCertificateDaoImpl.saveIntoGiftCertificateTags(giftCertificateId, tagId);
    }

}

package com.epam.esm.services;

import com.epam.esm.dao.TagDaoImpl;
import com.epam.esm.dto.TagDto;
import com.epam.esm.mapper.TagObjectObjectMapper;
import com.epam.esm.model.Tag;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * TagService class that extends Base Service which provide CRUD operations
 * Coming inputs and outgoing parameters will be dto classes
 *
 * @author Abduvohid Isroilov
 */
@Service
public class TagService extends BaseService<TagDaoImpl, TagObjectObjectMapper, Tag, TagDto> {

    /**
     * Constructor for Tag service
     *
     * @param repository TagDaoImpl
     * @param mapper
     */
    public TagService(TagDaoImpl repository, TagObjectObjectMapper mapper) {
        super(repository, mapper);
    }

    /**
     * Getting tag dto by its name
     *
     * @param name String
     * @return List<TagDto>
     */
    public List<TagDto> getTagsByName(String name) {
        return getRepository().getAllByName(name)
                .stream()
                .map(tag -> getMapper().modelToDto(tag))
                .collect(Collectors.toList());

    }

}

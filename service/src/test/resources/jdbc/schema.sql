CREATE SCHEMA IF NOT EXISTS "epam-esm_test";

create table gift_certificate
(
    id               bigint not null auto_increment,
    name             varchar(50),
    description      varchar(200),
    price            decimal(10, 2),
    duration         numeric,
    create_date      timestamp,
    last_update_date timestamp,
    PRIMARY KEY (id)
) ;


create table tag
(
    id   bigint not null auto_increment,
    name varchar(100),
    PRIMARY KEY (id)
) ;


create table gift_certificate_tags
(
    gift_certificate_id bigint not null,
    tag_id              bigint not null,
    FOREIGN KEY (gift_certificate_id) REFERENCES gift_certificate (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (gift_certificate_id, tag_id)
);
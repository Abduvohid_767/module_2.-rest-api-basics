package com.epam.esm;

import com.epam.esm.config.TestConfiguration;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.services.TagService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Abduvohid Isroilov
 * Testing Tag service layer
 * ApplicationContext will be loaded from the static using @SpringJUnitConfig
 */
@SpringJUnitConfig(TestConfiguration.class)
public class TagServiceTest {

    @Autowired
    TagService tagService;

    /**
     * This method will run before every @Test method in this class
     * Creating some tags when this method is starting
     */
    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);

        List<TagDto> tagDtos = Arrays.asList(
                new TagDto(1L, "tag1", new ArrayList<>()),
                new TagDto(2L, "tag2", new ArrayList<>())
        );

        tagService.create(tagDtos.get(0));
        tagService.create(tagDtos.get(1));

    }

    /**
     * Testing Tag's getAll() method
     */
    @Test
    public void testingTagGetAll() {
        Collection<TagDto> tagDtos = tagService.getAll();
        assertFalse(tagDtos.isEmpty());
    }

    /**
     * Testing Tag's create() method
     */
    @Test
    public void testingCreatingTag() {

        TagDto tagDto = new TagDto(3L, "tag3", new ArrayList<>());
        tagService.create(tagDto);
        TagDto checkingObject = tagService.getById(3L);
        assertNotNull(checkingObject);
        assertEquals(checkingObject.getName(), "tag3");
    }

    /**
     * Testing Tag's getById() method
     */
    @Test
    public void testingTagGetById() {

        TagDto tagDto = tagService.getById(1L);
        assertNotNull(tagDto);
        assertEquals(tagDto.getName(), "tag1");

    }

    /**
     * Testing Tag's deleteById() method
     */
    @Test
    public void testingTagDeleteById() {
        tagService.deleteById(2L);
        assertThrows(CustomNotFoundException.class, () -> tagService.getById(2L));
    }

}

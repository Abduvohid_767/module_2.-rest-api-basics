package com.epam.esm.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * App initializer class for using onStartup method for profile setting
 *
 * @author Abduvohid Isroilov
 */
@Configuration
public class AppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "dev");
        servletContext.setInitParameter(
                "spring.profiles.active", "dev");
    }
}

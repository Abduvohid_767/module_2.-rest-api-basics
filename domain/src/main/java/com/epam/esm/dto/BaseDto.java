package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Base model for the client connection
 *
 * @author Abduvohid Isroilov
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BaseDto {

    private String name;
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BaseDto(String name, Long id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseDto() {
    }

    public BaseDto(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BaseDto{" +
                "name='" + name + '\'' +
                '}';
    }
}

package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the dto class for exchanging data with client and service
 *
 * @author Abduvohid Isroilov
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TagDto extends BaseDto {

    @JsonProperty("giftCertificates")
    private List<GiftCertificateDto> giftCertificateDtos = new LinkedList<>();

    public TagDto() {
    }

    public TagDto(Long id, String name, List<GiftCertificateDto> giftCertificateDtos) {
        super(name, id);
        this.giftCertificateDtos = giftCertificateDtos;
    }

    public List<GiftCertificateDto> getGiftCertificateDtos() {
        return giftCertificateDtos;
    }

    public void setGiftCertificateDtos(List<GiftCertificateDto> giftCertificateDtos) {
        this.giftCertificateDtos = giftCertificateDtos;
    }

    @Override
    public String toString() {
        return "TagDto{" +
                "giftCertificateDtos=" + giftCertificateDtos +
                '}';
    }
}

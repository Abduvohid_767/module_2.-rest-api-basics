package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the dto class for exchanging data with client and service
 *
 * @author Abduvohid Isroilov
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GiftCertificateDto extends BaseDto {

    private String description;
    private Double price;
    private Integer duration;
    private String create_date;
    private String last_update_date;
    @JsonProperty("tags")
    private List<TagDto> tagDtos = new LinkedList<>();

    /**
     * No argument constructor for giftCertificate dto class
     */
    public GiftCertificateDto() {
    }

    public GiftCertificateDto(String name,
                              String description,
                              Double price,
                              Integer duration) {
        super(name);
        this.description = description;
        this.price = price;
        this.duration = duration;
    }

    /**
     * All argument constructor for gift certificate dto class
     *
     * @param name
     * @param description
     * @param price
     * @param duration
     * @param create_date
     * @param last_update_date
     * @param tagDtos
     */
    public GiftCertificateDto(Long id, String name, String description, Double price, Integer duration, String create_date, String last_update_date, List<TagDto> tagDtos) {
        super(name, id);
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.create_date = create_date;
        this.last_update_date = last_update_date;
        this.tagDtos = tagDtos;
    }

    public List<TagDto> getTagDtos() {
        return tagDtos;
    }

    public void setTagDtos(List<TagDto> tagDtos) {
        this.tagDtos = tagDtos;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(String last_update_date) {
        this.last_update_date = last_update_date;
    }

    @Override
    public String toString() {
        return "GiftCertificateDto" +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", duration=" + duration +
                ", create_date='" + create_date + '\'' +
                ", last_update_date='" + last_update_date + '\'' +
                '}';
    }
}

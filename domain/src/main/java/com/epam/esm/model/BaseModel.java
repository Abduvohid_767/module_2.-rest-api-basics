package com.epam.esm.model;

/**
 * Base model for the database objects
 *
 * @author Abduvohid Isroilov
 */
public class BaseModel {

    private Long id;
    private String name;

    public BaseModel(String name) {
        this.name = name;
    }

    public BaseModel(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public BaseModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

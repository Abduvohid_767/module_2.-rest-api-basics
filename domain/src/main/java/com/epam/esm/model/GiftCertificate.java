package com.epam.esm.model;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the model class for exchanging data with database
 *
 * @author Abduvohid Isroilov
 */
public class GiftCertificate extends BaseModel {

    private String description;
    private Double price;
    private Integer duration;
    private String create_date;
    private String last_update_date;
    private List<Tag> tags = new LinkedList<>();

    /**
     * No argument constructor for giftCertificate class
     */
    public GiftCertificate() {
    }

    /**
     * All argument constructor for GiftCertificate
     *
     * @param id
     * @param name
     * @param description
     * @param price
     * @param duration
     * @param create_date
     * @param last_update_date
     * @param tags
     */
    public GiftCertificate(Long id,
                           String name,
                           String description,
                           Double price,
                           Integer duration,
                           String create_date,
                           String last_update_date,
                           List<Tag> tags) {
        super(id, name);
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.create_date = create_date;
        this.last_update_date = last_update_date;
        this.tags = tags;
    }

    public GiftCertificate(String name,
                           String description,
                           Double price,
                           Integer duration) {
        super(name);
        this.description = description;
        this.price = price;
        this.duration = duration;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(String last_update_date) {
        this.last_update_date = last_update_date;
    }

    @Override
    public String toString() {
        return "GiftCertificate{" +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", duration=" + duration +
                ", create_date='" + create_date + '\'' +
                ", last_update_date='" + last_update_date + '\'' +
                '}';
    }
}

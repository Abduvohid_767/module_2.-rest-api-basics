package com.epam.esm.model;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the model class for exchanging data with database
 *
 * @author Abduvohid Isroilov
 */
public class Tag extends BaseModel {

    private List<GiftCertificate> giftCertificates = new LinkedList<>();

    public Tag() {
    }

    public Tag(Long id, String name, List<GiftCertificate> giftCertificates) {
        super(id, name);
        this.giftCertificates = giftCertificates;
    }

    public List<GiftCertificate> getGiftCertificates() {
        return giftCertificates;
    }

    public void setGiftCertificates(List<GiftCertificate> giftCertificates) {
        this.giftCertificates = giftCertificates;
    }
}

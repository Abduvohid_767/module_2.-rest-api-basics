package com.epam.esm;

import com.epam.esm.dao.TagDaoImpl;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.model.Tag;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Abduvohid Isroilov
 * Integration Testing Tag repository layer
 * ApplicationContext will be loaded using @SpringJUnitConfig
 */
@Category(TagRepoIntegrationTestCategory.class)
@SpringJUnitConfig(TestConfiguration.class)
public class TagRepoIntegrationTest {

    @Autowired
    TagDaoImpl tagDao;

    /**
     * This method will run before every @Test method in this class
     * Creating some tag when this method is starting
     */
    @BeforeEach
    public void onStartup() {
        System.out.println("TagRepoIntegrationTest is running");
        MockitoAnnotations.initMocks(this);

        List<Tag> tags = Arrays.asList(
                new Tag(1L, "tag1", new ArrayList<>()),
                new Tag(2L, "tag2", new ArrayList<>())
        );

        tagDao.save(tags.get(0));
        tagDao.save(tags.get(1));

    }

    /**
     * Testing TagDoaImpl's getAll() method
     */
    @Test
    public void testTagGetAll() {
        Collection<Tag> tags = tagDao.getAll();
        assertFalse(tags.isEmpty());
    }

    /**
     * Testing TagDoaImpl's getById() method
     */
    @Test
    public void testTagGetById() {
        Optional<Tag> tag = getTagById(1L);
        assertTrue(tag.isPresent());
        assertEquals(tag.get().getName(), "tag1");
    }

    /**
     * Testing TagDoaImpl's save() method
     */
    @Test
    public void testTagSave() {
        Tag tag = new Tag(3L, "tag3", new ArrayList<>());
        tagDao.save(tag);
        Optional<Tag> checkingObject = getTagById(3L);
        assertTrue(checkingObject.isPresent());
        assertEquals(checkingObject.get().getName(), "tag3");
    }

    /**
     * Testing TagDoaImpl's deleteById() method
     */
    @Test
    public void testTagDeleteById() {
        tagDao.delete(2L);
        assertThrows(CustomNotFoundException.class, () -> getTagById(2L));
    }

    /**
     * Testing TagDoaImpl's getByName() method
     */
    @Test
    public void testTagByName() {
        Optional<Tag> tag = tagDao.getByName("tag1");
        assertTrue(tag.isPresent());
    }

    private Optional<Tag> getTagById(long id) {
        return tagDao.get(id);
    }

}

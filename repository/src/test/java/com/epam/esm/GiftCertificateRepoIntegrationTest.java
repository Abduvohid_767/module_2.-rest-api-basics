package com.epam.esm;

import com.epam.esm.dao.GiftCertificateDaoImpl;
import com.epam.esm.dao.TagDaoImpl;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Abduvohid Isroilov
 * Integration Testing Gift certificate repository layer
 * ApplicationContext will be loaded using @SpringJUnitConfig
 */
@Category(GiftCertificateRepoIntegrationTestCategory.class)
@SpringJUnitConfig(TestConfiguration.class)
public class GiftCertificateRepoIntegrationTest {

    @Autowired
    GiftCertificateDaoImpl giftCertificateDao;

    @Autowired
    TagDaoImpl tagDao;

    /**
     * This method will run before every @Test method in this class
     * Creating some gift certificate and tag when this method is starting
     */
    @BeforeEach
    public void onStartup() {
        System.out.println("GiftCertificateRepoIntegrationTest is running");
        MockitoAnnotations.initMocks(this);

        List<GiftCertificate> giftCertificates = Arrays.asList(
                new GiftCertificate(1L, "gift1", "1", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>()),
                new GiftCertificate(2L, "gift2", "2", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>())
        );

        List<Tag> tags = Arrays.asList(
                new Tag(1L, "tag1", new ArrayList<>()),
                new Tag(2L, "tag2", new ArrayList<>())
        );

        giftCertificateDao.save(giftCertificates.get(0));
        giftCertificateDao.save(giftCertificates.get(1));

        tagDao.save(tags.get(0));
        tagDao.save(tags.get(1));

    }

    /**
     * Testing GiftCertificateDaoImpl's getAll() method
     */
    @Test
    public void testGiftCertificateGetAll() {
        Collection<GiftCertificate> giftCertificates = giftCertificateDao.getAll();
        assertFalse(giftCertificates.isEmpty());
    }

    /**
     * Testing GiftCertificateDaoImpl's getById() method
     */
    @Test
    public void testGiftCertificateGetById() {
        Optional<GiftCertificate> giftCertificate = getGiftCertificateById(1L);
        assertTrue(giftCertificate.isPresent());
        assertEquals(giftCertificate.get().getDescription(), "1");
    }

    /**
     * Testing GiftCertificateDaoImpl's save() method
     */
    @Test
    public void testGiftCertificateSave() {
        GiftCertificate giftCertificate = new GiftCertificate(3L, "gift3", "3", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>());
        giftCertificateDao.save(giftCertificate);
        Optional<GiftCertificate> checkingObject = getGiftCertificateById(3L);
        assertTrue(checkingObject.isPresent());
        assertEquals(checkingObject.get().getDescription(), "3");
    }


    /**
     * Testing GiftCertificateDaoImpl's updateById() method
     */
    @Test
    public void testGiftCertificateUpdateById() {
        GiftCertificate giftCertificate = new GiftCertificate(2L, "gift2", "2_UPDATED", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>());
        giftCertificateDao.updateById(giftCertificate, 2L);
        Optional<GiftCertificate> checkingObject = getGiftCertificateById(2L);
        assertTrue(checkingObject.isPresent());
        assertEquals(checkingObject.get().getDescription(), "2_UPDATED");
    }

    /**
     * Testing GiftCertificateDaoImpl's deleteById() method
     */
    @Test
    public void testGiftCertificateDeleteById() {
        giftCertificateDao.delete(2L);
        assertThrows(CustomNotFoundException.class, () -> getGiftCertificateById(2L));
    }

    /**
     * Testing GiftCertificateDaoImpl's batchUpdate() method
     */
    @Test
    public void testGiftCertificateBatchUpdate() {
        List<GiftCertificate> giftCertificates = Arrays.asList(
                new GiftCertificate(1L, "gift1", "1_UPDATED", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>()),
                new GiftCertificate(2L, "gift2", "2_UPDATED", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>())
        );

        giftCertificateDao.batchUpdate(giftCertificates);
        Optional<GiftCertificate> checkingObject = getGiftCertificateById(1L);
        assertTrue(checkingObject.isPresent());
        assertEquals(checkingObject.get().getDescription(), "1_UPDATED");

        checkingObject = getGiftCertificateById(2L);
        assertTrue(checkingObject.isPresent());
        assertEquals(checkingObject.get().getDescription(), "2_UPDATED");
    }

    /**
     * Testing GiftCertificateDaoImpl's getAllGiftCertificateByTagName() method
     */
    @Test
    public void testGiftCertificateByTagName() {
        List<GiftCertificate> giftCertificate = giftCertificateDao.getAllGiftCertificateByTagName("tag1");
        assertNull(giftCertificate.get(0));
    }

    /**
     * Testing GiftCertificateDaoImpl's saveIntoGiftCertificateTags() method
     */
    @Test
    public void testGiftCertificateSaveIntoGiftCertificateTags() {
        giftCertificateDao.saveIntoGiftCertificateTags(1L, 1L);
        Optional<GiftCertificate> giftCertificate = getGiftCertificateById(1L);
        assertTrue(giftCertificate.isPresent());
        assertNotNull(giftCertificate.get().getTags().get(0));
    }

    /**
     * Testing GiftCertificateDaoImpl's getAllGiftCertificateByDescription() method
     */
    @Test
    public void testGiftCertificateByDescription() {
        List<GiftCertificate> giftCertificate = giftCertificateDao.getAllGiftCertificateByDescription("1");
        assertFalse(giftCertificate.isEmpty());
    }

    /**
     * Testing GiftCertificateDaoImpl's getByName() method
     */
    @Test
    public void testGiftCertificateByName() {
        Optional<GiftCertificate> giftCertificate = giftCertificateDao.getByName("gift1");
        assertTrue(giftCertificate.isPresent());
    }

    /**
     * Testing GiftCertificateDaoImpl's getAllGiftCertificateOrderByName() method
     */
    @Test
    public void testGiftCertificateOrderByName() {
        List<GiftCertificate> giftCertificate = giftCertificateDao.getAllGiftCertificateOrderByName(false);
        assertNotNull(giftCertificate);
    }


    private Optional<GiftCertificate> getGiftCertificateById(long id) {
        return giftCertificateDao.get(id);
    }

}

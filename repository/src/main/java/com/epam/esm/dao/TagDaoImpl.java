package com.epam.esm.dao;

import com.epam.esm.database.TagQueries;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.mapper.TagMapper;
import com.epam.esm.model.Tag;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;


/**
 * TagDaoImpl class for the connection with database
 *
 * @author Abduvohid Isroilov
 */
@Component
public class TagDaoImpl extends BaseDaoImpl<Tag, TagQueries, TagMapper> {

    public TagDaoImpl(TagQueries queries, TagMapper mapper, JdbcTemplate jdbcTemplate) {
        super(queries, mapper, jdbcTemplate);
    }

    /**
     * Saving new Tag
     *
     * @param tag Tag
     * @return int generated unique id
     */
    @Override
    @Transactional
    public long save(Tag tag) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        getJdbcTemplate().update(
                con -> {
                    PreparedStatement ps = con.prepareStatement(getQueries().save(), Statement.RETURN_GENERATED_KEYS);
                    if (tag.getName() != null)
                        ps.setString(1, tag.getName());
                    return ps;
                }, keyHolder);

        if (keyHolder.getKey() != null)
            return keyHolder.getKey().longValue();

        return 0;
    }

    /**
     * Getting tags by its name
     *
     * @param name String
     * @return List<Tag>
     */
    public List<Tag> getAllByName(String name) {
        try {

            return getJdbcTemplate().query(getQueries().getByName(), getMapper(), name);
        } catch (Exception ex) {
            throw new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH NAME ( " + name + " ) ");
        }
    }
}

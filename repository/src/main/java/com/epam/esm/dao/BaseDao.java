package com.epam.esm.dao;

import com.epam.esm.model.BaseModel;

import java.util.Collection;
import java.util.Optional;

/**
 * Base dao interface for data exchange with database
 * Every dao class should implement this BaseDao methods
 *
 * @param <T>
 * @author Abduvohid Isroilov
 */
public interface BaseDao<T> {

    Optional<T> get(long id);

    Optional<T> getByName(String name);

    Collection<T> getAll();

    long save(T t);

    boolean delete(long id);

}

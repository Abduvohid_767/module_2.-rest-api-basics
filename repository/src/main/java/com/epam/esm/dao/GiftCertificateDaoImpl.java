package com.epam.esm.dao;

import com.epam.esm.database.GiftCertificateQueries;
import com.epam.esm.exception.BadRequestException;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.exception.DatabaseException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.mapper.GiftCertificateMapper;
import com.epam.esm.model.GiftCertificate;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * GiftCertificateDoaImpl class for the connection with database
 *
 * @author Abduvohid Isroilov
 */
@Component
public class GiftCertificateDaoImpl extends BaseDaoImpl<GiftCertificate, GiftCertificateQueries, GiftCertificateMapper> {

    public GiftCertificateDaoImpl(GiftCertificateQueries queries, GiftCertificateMapper mapper, JdbcTemplate jdbcTemplate) {
        super(queries, mapper, jdbcTemplate);
    }

    /**
     * Saving new GiftCertificate
     *
     * @param giftCertificate GiftCertificate
     * @return int generated unique id
     */
    @Transactional
    @Override
    public long save(GiftCertificate giftCertificate) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        getJdbcTemplate().update(
                con -> {
                    PreparedStatement ps = con.prepareStatement(getQueries().save(), Statement.RETURN_GENERATED_KEYS);
                    if (giftCertificate.getName() != null)
                        ps.setString(1, giftCertificate.getName());
                    if (giftCertificate.getDescription() != null)
                        ps.setString(2, giftCertificate.getDescription());
                    ps.setDouble(3, giftCertificate.getPrice());
                    ps.setInt(4, giftCertificate.getDuration());
                    if (giftCertificate.getCreate_date() != null)
                        ps.setTimestamp(5, Timestamp.valueOf(giftCertificate.getCreate_date()));
                    if (giftCertificate.getLast_update_date() != null)
                        ps.setTimestamp(6, Timestamp.valueOf(giftCertificate.getLast_update_date()));
                    return ps;
                }, keyHolder);

        if (keyHolder.getKey() != null)
            return keyHolder.getKey().longValue();
        return 0;
    }

    /**
     * Update gift certificate by its id
     *
     * @param giftCertificate GiftCertificate
     * @param id              long
     * @return boolean
     */
    @Transactional
    public boolean updateById(GiftCertificate giftCertificate, long id) {

        List<Object> objects = new ArrayList<>();

        if (giftCertificate.getName() != null)
            objects.add(giftCertificate.getName());

        if (giftCertificate.getDescription() != null)
            objects.add(giftCertificate.getDescription());

        if (giftCertificate.getDuration() != null)
            objects.add(giftCertificate.getDuration());

        if (giftCertificate.getPrice() != null)
            objects.add(giftCertificate.getPrice());

        if (giftCertificate.getCreate_date() != null)
            objects.add(Timestamp.valueOf(giftCertificate.getCreate_date()));

        if (giftCertificate.getLast_update_date() != null)
            objects.add(Timestamp.valueOf(giftCertificate.getLast_update_date()));

        objects.add(id);

        return getJdbcTemplate().update(getQueries().updateById(giftCertificate), objects.toArray()) == 1;
    }

    /**
     * Update batch of gift certificates
     *
     * @param giftCertificateList int []
     * @return int[]
     */
    public int[] batchUpdate(List<GiftCertificate> giftCertificateList) {

        return getJdbcTemplate().batchUpdate(
                getQueries().batchUpdate(),
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        if (giftCertificateList.get(i).getId() == null)
                            throw new BadRequestException("Could not update without id");
                        if (giftCertificateList.get(i).getName() != null)
                            ps.setString(1, giftCertificateList.get(i).getName());
                        if (giftCertificateList.get(i).getDescription() != null)
                            ps.setString(2, giftCertificateList.get(i).getDescription());
                        ps.setInt(3, giftCertificateList.get(i).getDuration());
                        ps.setDouble(4, giftCertificateList.get(i).getPrice());
                        if (giftCertificateList.get(i).getCreate_date() != null)
                            ps.setTimestamp(5, Timestamp.valueOf(giftCertificateList.get(i).getCreate_date()));
                        if (giftCertificateList.get(i).getLast_update_date() != null)
                            ps.setTimestamp(6, Timestamp.valueOf(giftCertificateList.get(i).getLast_update_date()));
                        ps.setLong(7, giftCertificateList.get(i).getId());

                    }

                    @Override
                    public int getBatchSize() {
                        return giftCertificateList.size();
                    }
                });
    }

    /**
     * Getting gift certificate by tag name
     *
     * @param tagName String
     * @return List<GiftCertificate>
     */
    public List<GiftCertificate> getAllGiftCertificateByTagName(String tagName) {
        try {
            return getJdbcTemplate().query(getQueries().getGiftCertificateByTagName(), new GiftCertificateMapper(), tagName);
        } catch (Exception ex) {
            throw new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH TAG NAME ( " + tagName + " ) ");

        }
    }

    /**
     * Getting gift certificate by gift certificate description
     *
     * @param description String
     * @return List<GiftCertificate>
     */
    public List<GiftCertificate> getAllGiftCertificateByDescription(String description) {
        try {
            return getJdbcTemplate().query(getQueries().getByDescription(), new GiftCertificateMapper(), description);
        } catch (Exception ex) {
            throw new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH DESCRIPTION ( " + description + " ) ");
        }
    }

    /**
     * Method for getting all gift certificate and order them by name in given order
     *
     * @param isASC boolean
     * @return List<GiftCertificate>
     */
    public List<GiftCertificate> getAllGiftCertificateOrderByName(boolean isASC) {
        return getJdbcTemplate().query(getQueries().getAllOrderByName(isASC), new GiftCertificateMapper());
    }

    /**
     * Method for getting all gift certificate and order them by create date in given order
     *
     * @param isASC boolean
     * @return List<GiftCertificate>
     */
    public List<GiftCertificate> getAllGiftCertificateOrderByCreateDate(boolean isASC) {
        return getJdbcTemplate().query(getQueries().getAllOrderByCreateDate(isASC), new GiftCertificateMapper());
    }

    /**
     * Method for getting all gift certificate and order them by last update date in given order
     *
     * @param isASC boolean
     * @return List<GiftCertificate>
     */
    public List<GiftCertificate> getAllGiftCertificateOrderByLastUpdateDate(boolean isASC) {
        return getJdbcTemplate().query(getQueries().getAllOrderByLastUpdateDate(isASC), new GiftCertificateMapper());
    }

    /**
     * Method for getting all gift certificate and order them by name and create date in given order
     *
     * @param isASCCreateDate boolean
     * @param isASCName       boolean
     * @return List<GiftCertificate>
     */
    public List<GiftCertificate> getAllGiftCertificateOrderByCreateDateAndName(boolean isASCCreateDate, boolean isASCName) {
        return getJdbcTemplate().query(getQueries().getAllOrderByCreateDateAndName(isASCCreateDate, isASCName), new GiftCertificateMapper());
    }

    /**
     * Method for getting all gift certificate and order them by name and last update date in given order
     *
     * @param isASCLastUpdate boolean
     * @param isASCName       boolean
     * @return List<GiftCertificate>
     */
    public List<GiftCertificate> getAllGiftCertificateOrderByLastUpdateDateAndName(boolean isASCLastUpdate, boolean isASCName) {
        return getJdbcTemplate().query(getQueries().getAllOrderByLastUpdateDateAndName(isASCLastUpdate, isASCName), new GiftCertificateMapper());
    }

    /**
     * Method for inserting data into gift certificate tags in order to provide many to many relationship
     *
     * @param giftCertificateId long
     * @param tagId             long
     * @return int
     */
    public int saveIntoGiftCertificateTags(long giftCertificateId, long tagId) {
        try {
            return getJdbcTemplate().update(getQueries().insertIntoGiftCertificateTags(), giftCertificateId, tagId);
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

}

package com.epam.esm.mapper;

import com.epam.esm.exception.DatabaseException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

public class BaseMapperHelper {

    protected GiftCertificate getGiftCertificate(ResultSet resultSet) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        GiftCertificate giftCertificate = new GiftCertificate();

        try {

            if (resultSet.getLong("id") == 0)
                return null;

            giftCertificate.setId(resultSet.getLong("id"));

            if (resultSet.getString("name") != null)
                giftCertificate.setName(resultSet.getString("name"));

            if (resultSet.getString("description") != null)
                giftCertificate.setDescription(resultSet.getString("description"));

            giftCertificate.setDuration(resultSet.getInt("duration"));

            giftCertificate.setPrice(resultSet.getDouble("price"));

            if (resultSet.getTimestamp("create_date") != null)
                giftCertificate.setCreate_date(formatter.format(resultSet.getTimestamp("create_date")));

            if (resultSet.getTimestamp("last_update_date") != null)
                giftCertificate.setLast_update_date(formatter.format(resultSet.getTimestamp("last_update_date")));
        } catch (SQLException sqlException) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + sqlException.getSQLState() + " ) ");
        }
        return giftCertificate;
    }

    protected Tag getTag(ResultSet resultSet) {
        Tag tag = new Tag();
        try {

            if (resultSet.getLong("tag_id") == 0)
                return null;

            tag.setId(resultSet.getLong("tag_id"));

            if (resultSet.getString("tag_name") != null)
                tag.setName(resultSet.getString("tag_name"));
        } catch (SQLException sqlException) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + sqlException.getSQLState() + " ) ");
        }
        return tag;
    }

}

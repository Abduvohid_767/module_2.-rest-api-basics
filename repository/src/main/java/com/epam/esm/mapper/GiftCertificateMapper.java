package com.epam.esm.mapper;


import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;

/**
 * GiftCertificateMapper class for map coming data from database to java object (GiftCertificate)
 *
 * @author Abduvohid Isroilov
 */
@Component
public class GiftCertificateMapper implements BaseMapper<GiftCertificate> {

    /**
     * Implemented mapRow(ResultSet resultSet, int rowNum) method from RowMapper
     *
     * @param resultSet ResultSet
     * @param rowNum    int
     * @return GiftCertificate
     */
    @Override
    public GiftCertificate mapRow(ResultSet resultSet, int rowNum) {

        BaseMapperHelper baseMapperHelper = new BaseMapperHelper();

        GiftCertificate giftCertificate = baseMapperHelper.getGiftCertificate(resultSet);

        if (baseMapperHelper.getGiftCertificate(resultSet) != null) {
            Tag tag = baseMapperHelper.getTag(resultSet);
            giftCertificate.getTags().add(tag);
        }

        return giftCertificate;
    }

}

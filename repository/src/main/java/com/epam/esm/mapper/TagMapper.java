package com.epam.esm.mapper;

import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * TagMapper class for map coming data from database to java object (Tag)
 *
 * @author Abduvohid Isroilov
 */
@Component
public class TagMapper implements BaseMapper<Tag> {

    /**
     * Implemented mapRow(ResultSet resultSet, int rowNum) method from RowMapper
     *
     * @param rs ResultSet
     * @param rowNum int
     * @return Tag
     */
    @Override
    public Tag mapRow(ResultSet rs, int rowNum) {

        BaseMapperHelper baseMapperHelper = new BaseMapperHelper();

        Tag tag = baseMapperHelper.getTag(rs);

        if (baseMapperHelper.getGiftCertificate(rs) != null) {
            GiftCertificate giftCertificate = baseMapperHelper.getGiftCertificate(rs);
            tag.getGiftCertificates().add(giftCertificate);
        }

        return tag;
    }
}

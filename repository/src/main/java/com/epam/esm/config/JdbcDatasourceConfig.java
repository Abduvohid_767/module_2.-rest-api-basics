package com.epam.esm.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * Jdbc Config class created for the connection with mysql database
 * Inject DatabaseConfigData class where all static fields are defined
 *
 * @author Abduvohid Isroilov
 */
@Configuration
public class JdbcDatasourceConfig {

    private static final Logger LOG = LoggerFactory.getLogger(JdbcDatasourceConfig.class);

    private final DatabaseConfigData databaseConfigData;

    public JdbcDatasourceConfig(DatabaseConfigData databaseConfigData) {
        this.databaseConfigData = databaseConfigData;
    }

    /**
     * Jdbc datasource setting for @Primary database resources
     *
     * @return Datasource
     */
    @Bean(name = "epam-esm-datasource")
    @Primary
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(databaseConfigData.getDriverClassName());
        dataSource.setUrl(databaseConfigData.getUrl());
        dataSource.setUsername(databaseConfigData.getUsername());
        dataSource.setPassword(databaseConfigData.getPassword());

        return dataSource;
    }

    /*
     If used embedded H2 database here simple sql scripts
     */
//    @Bean
//    public DataSource dataSource() {
//        return new EmbeddedDatabaseBuilder()
//                .setType(EmbeddedDatabaseType.H2)
//                .addScript("classpath:jdbc/schema.sql").build();
//    }

}